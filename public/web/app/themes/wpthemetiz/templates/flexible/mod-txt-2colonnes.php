<div class="grid-container">
<div class="mod-txt-2colonnes grid-x">
                <div class="cell small-6 large-6 medium-6">
                        <div class="picture_txt_2colonnes"><?= wp_get_attachment_image(get_sub_field('picture_txt_2colonnes'), 'full' ) ?></div>
                </div>
                <div class="cell small-6 large-6 medium-6">
                        <div class='mod2text'>
                                <div class="feature_title "style="display:inline-block;" >
                                <?php  the_sub_field('h2_title_feature_2colonnes'); ?>
                       
                        <div class="subtitle">
                                <?php  the_sub_field('h2_title_2colonnes'); ?></div>
                        </div>
                        <div class="small-6 medium-4 large-3 columns paragraph">
                                <?php  the_sub_field('paragraph_txt_2colones'); ?>
                        </div>
                        </div>
                </div> 
</div>
</div>
  
