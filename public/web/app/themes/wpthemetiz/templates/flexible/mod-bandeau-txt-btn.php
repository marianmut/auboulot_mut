<div class="orange-over2">
              </div>
<div class="mod-bandeau__image"><?= wp_get_attachment_image(get_sub_field('picture_txt_btn'), 'full' ) ?></div>
<div class="mod-bandeau__text">
<div class="mod-bandeau__title "><?php  the_sub_field('h2title_txt_btn'); ?></div>
<div class="mod-bandeau__paragraph "><?php  the_sub_field('paragraph_txt_btn'); ?></div>
<div class="mod-bandeau__buttons grid-x">
                    <div class="mod-bandeau__button">
                                            <?php
                                            $link = get_sub_field('button_black_txt_btn');
                        
                                            if ($link) :
                                                $link_url = $link['url'];
                                            $link_title = $link['title'];
                                            $link_target = $link['target'] ? $link['target'] : '_self';
                                            ?>
                                                <a class="mod-bandeau__link"href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                                            <?php endif; ?>
                                            
                        </div>
                    <div class="mod-bandeau__button2">
                        <?php
                        $link = get_sub_field('button_black_txt_btn2');
    
                        if ($link) :
                            $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                            <a class="mod-bandeau__link"href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                        
    </div>
</div>

