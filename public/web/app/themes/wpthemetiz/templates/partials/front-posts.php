 <!-- Childs START -->

 <section class="posts_container">
 <h2 class="posts__title">
   <p class="feature_title">Les dernières actualités.</p>
    </h2>
            <p> <?php the_field('titlechilds'); ?> </p>
            <article style="margin-left: -12%;">
    <div class="childfull">
      <div class="grid-x">
<?php

 $args = array(
    'posts_per_page'   => 3,
	'offset'           => 0,
	'cat'         => '',
	'category_name'    => '',
	'orderby'          => 'date',
	'order'            => 'DESC',
	'include'          => '',
	'exclude'          => '',
	'meta_key'         => '',
	'meta_value'       => '',
	'post_type'        => 'post',
	'post_mime_type'   => '',
	'post_parent'      => '',
	'author'	   => '',
	'author_name'	   => '',
	'post_status'      => 'publish',
	'suppress_filters' => true,
	'fields'           => '',
); 


$parent = new WP_Query( $args );

if ( $parent->have_posts() ) : ?>

    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
        <div class="post__card ">
            <div id="parent-<?php the_ID(); ?>" class="cell medium-6 large-3 small-6 post__page">

               <div class='post__img'> <?php the_post_thumbnail( 'thumbnail' );  ?> </div>

                <div class="post__date">
                        <?php $my_date = the_date( 'd/m/Y', false );
                        echo $my_date;;?>
                        </div>

                  <div class="post__title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                </div>

                        </div>   
                        <div class="post__excerpt"> 
                        <?php the_excerpt(); ?>
                        </div>

                            <?php 
                            $link = get_field('post_link'); 
                            $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
                                <a class="post__button" href="<?php echo esc_url(the_permalink()); ?>" target="<?php echo esc_attr($link_target); ?>">Lire la suite</a>
                        </div>
    <?php endwhile; ?>
    
<?php endif; wp_reset_postdata(); ?>


</div>
</div>
</div>
</article>
<div class="post__buton">
                            <?php 
                            $link = get_field('posts-link'); 
                            $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
                                <a class="post__last__button" href="<?php echo esc_url(the_permalink()); ?>" target="<?php echo esc_attr($link_target); ?>">Voir tous les articles</a>
                        </div>
</section>
 <!-- Childs Stop -->