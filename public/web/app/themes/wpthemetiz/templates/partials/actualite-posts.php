 <div class="actualite_posts grid-container">
     <div class="grid-x">
                <?php

                $args = array(
                    'posts_per_page'   => 5,
                    'offset'           => 0,
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'post_type'        => 'post',
                    'post_status'      => 'publish',
                    'suppress_filters' => true,
                ); 


                $parent = new WP_Query( $args );

                if ( $parent->have_posts() ) : ?>

                    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

                                           
                                <div class='cell medium-12 large-6 small-6'>
                                <?php the_post_thumbnail( 'single-post-thumbnail' ); ?>   
                                 </div>
                <div class="article__card cell medium-12 large-6 small-6" >
                        <div class="post__date_a feature_title ">
                                    <?php $my_date = the_date( 'd/m/Y', false );
                                    echo $my_date;;?>
                        </div>
                        <p class="article__title "><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></p>
                            <div class="article__page">                      
                                       </div>   
                                        <div class="article__excerpt"> 
                                        <?php the_excerpt(); ?>
                                        </div>

                                        <div class="post__buttons_actualite">
                                            <?php 
                                            $link = get_field('post_link'); 
                                            $link_url = $link['url'];
                                                $link_title = $link['title'];
                                                $link_target = $link['target'] ? $link['target'] : '_self';
                                                ?>
                                                <a class="post__button_actualite" href="<?php echo esc_url(the_permalink()); ?>" target="<?php echo esc_attr($link_target); ?>">Lire la suite</a>
                                        </div>
                                        </div>
                    <?php endwhile; ?>
                    
                <?php endif; wp_reset_postdata(); ?>

            </div>            
                </div>
            </div>
                
</div>