
<section class='partner__slider grid-container'>
<div class="feature_title"><?php  the_field('stitle_partner'); ?></div>
<div class="slider">
<?php
$images = get_field('slider_partner');
if( $images ): ?>
 
   <div class="slider-nav">
        
            <?php foreach( $images as $image ): ?>
                <div>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    
                </div>
            <?php endforeach; ?>
    </div>
<?php endif; 
?>            
</div>
<div class="post__buton">
                            <?php 
                            $link = get_field('posts-link'); 
                            $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
                                <a class="post__last__button" href="<?php echo esc_url(the_permalink()); ?>" target="<?php echo esc_attr($link_target); ?>">Voir tous les articles</a>
                        </div>
</section>
</section>