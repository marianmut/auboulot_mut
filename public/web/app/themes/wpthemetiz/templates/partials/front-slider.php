            <div class="slider">
              <div class="orange-over">
              </div>
                    <?php if( have_rows('slider')):?>
                        <ul class="single-item">
                            <?php while ( have_rows('slider') ) : the_row(); ?>
                                <li>
                                    <?php
                                        echo wp_get_attachment_image(get_sub_field('simage'), 'full', false, ['class' => 'slider']);
                                   ?>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            
</div>