<header class="banner">
  <div class="container">
    <div class='header__top grid-x' >
    <div class='logo1'>
    <a class="brand_strasb" href="<?= esc_url("www.strasbourg.eu"); ?>"><?= wp_get_attachment_image( get_field('logo', 'option'), 'full' ) ?></a>
    </div>
    <div class='logo2'>
    <a  href="<?= esc_url("https://cadr67.fr"); ?>"><?= wp_get_attachment_image( get_field('logo2', 'option'), 'full' ) ?></a>
    </div>
</div>
<div class="grid-container">
<nav id="myTopnav" class="nav-menu" role="navigation">
<button class="h__button">Mon compte<i class="fas fa-bicycle"></i> </button>

<button class="menu-toggle"><i class="fa fa-bars"></i></button>

<nav id="primary-menu"  role="navigation">       
            
            <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'menu_class' => 'nav-menu' , 'exclude' => '20') ); ?>

</nav>

</header>

  </div>
