<div class="grid-x grid-margin-x grid-padding-y">
  <?php foreach ($lastPosts as $_post) : ?>
  <div class="cell small-12 medium-6">
    <?php
      global $post;
      $post = $_post;
      setup_postdata($post);
      get_template_part('templates/content');
      wp_reset_postdata();
    ?>
  </div>
  <?php endforeach; ?>
</div>
