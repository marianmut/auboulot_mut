<footer class="content-info">
  <div class="container">
  <div class='header__top grid-x' >
    <div class='logos'>
         <a class="brand_strasb" href="<?= esc_url("www.strasbourg.eu"); ?>"><?= wp_get_attachment_image( get_field('logo', 'option'), 'full' ) ?></a>
         <div class='logo2'>
    <a  href="<?= esc_url("https://cadr67.fr"); ?>"><?= wp_get_attachment_image( get_field('logo2', 'option'), 'full' ) ?></a>
    </div>
    <div class='logo3'>
    <a  href="<?= esc_url("https://cadr67.fr"); ?>"><?= wp_get_attachment_image( get_field('logo3', 'option'), 'full' ) ?></a>
    </div>
    
    </div>
  
  </div>
  <div class="f__links grid-x">
<?php
if (have_rows('f__links', 'option')) :

  while (have_rows('f__links', 'option')) : the_row();
?>
        <li>
              <?php 

              $link = get_sub_field('f_link');

              if ($link) :
                $link_url = $link['url'];
              $link_title = $link['title'];
              $link_target = $link['target'] ? $link['target'] : '_self';
              ?>
              <a  href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
            <?php endif; ?>
          </li>
<?php endwhile; ?>

  <?php else : endif; ?>
  <div class="f__tiz">
            <?php echo wp_get_attachment_image( get_field('logotiz','option'), 'full' );?>  
      </div>
</div>

</footer>
