
<?php // open the WordPress loop

// are there any rows within within our flexible content?
if( have_rows('flexible') ): 

  // loop through all the rows of flexible content
  while ( have_rows('flexible') ) : the_row();

  if( get_row_layout() == 'mod-txt-2colonnes' ):
    get_template_part('templates/flexible/mod-txt-2colonnes');


  elseif( get_row_layout() == 'mod-bandeau-txt-btn' ):
    get_template_part('templates/flexible/mod-bandeau-txt-btn');

  endif;
  endwhile; // close the loop of flexible content
endif; // close flexible content conditional

