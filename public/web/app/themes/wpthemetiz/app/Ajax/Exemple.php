<?php

namespace App\Ajax;

class Exemple extends Ajax {

  public function showExemple()
  {
    $exemples = [
      ['exemple' => 'exemple']
    ];

    echo $this->display($events);
    die();
  }

  public function load()
  {
    $this->init('showExemple');
  }

  private function display($exemple)
  {
    $exemple->the_post();
    return $this->render('exemple');
  }
}
