<?php

namespace App\CustomPostTypes;

class Manager {

  private $cpts = [];

  public function add($cpt)
  {
    $this->cpts[] = $cpt;
  }

  public function registers()
  {
    foreach($this->cpts as $cpt) {
      $cpt->register();
    }
  }
}
