<?php

namespace App\Override;

use App\Core\Render;

class Gallery {
  use Render;

  private $path = 'templates/override/';

  public function hooks()
  {
    add_filter('post_gallery', [$this, 'postGallery'], 10, 3);
  }

  public function postGallery($output, $attr, $instance)
  {
    global $post;

    if (!empty($attr['ids'])) {
      if (empty($attr['orderby'])) {
        $attr['orderby'] = 'post__in';
      }
      $attr['include'] = $attr['ids'];
    }

    $html5 = current_theme_supports('html5', 'gallery');
    $atts = shortcode_atts(array(
      'order' => 'ASC',
      'orderby' => 'menu_order ID',
      'id' => $post ? $post->ID : 0,
      'itemtag' => $html5 ? 'figure' : 'dl',
      'icontag' => $html5 ? 'div' : 'dt',
      'captiontag' => $html5 ? 'figcaption' : 'dd',
      'columns' => 3,
      'size' => 'thumbnail',
      'include' => '',
      'exclude' => '',
      'link' => ''
    ), $attr, 'gallery');

    $id = intval($atts['id']);

    // Get Attachments
    if (!empty($atts['include'])) {
      $_attachments = get_posts(array('include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby']));

      $attachments = array();
      foreach ($_attachments as $key => $val) {
        $attachments[$val->ID] = $_attachments[$key];
      }
    } elseif (!empty($atts['exclude'])) {
      $attachments = get_children(array('post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby']));
    } else {
      $attachments = get_children(array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby']));
    }

    if (empty($attachments)) {
      return '';
    }

    if (is_feed()) {
      $output = "\n";
      foreach ($attachments as $att_id => $attachment) {
        $output .= wp_get_attachment_link($att_id, $atts['size'], true) . "\n";
      }
      return $output;
    }

    $columns = intval($atts['columns']);
    $image_metas = [];

    foreach ($attachments as $id => $attachment) {
      $image_metas[$id] = wp_get_attachment_metadata($id);
    }

    return $this->render('gallery', compact('attachments', 'image_metas', 'atts'));
  }





}
