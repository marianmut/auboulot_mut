/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

 

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };
  jQuery(document).ready(function($){
    
   $('.slider-nav').slick({
    infinite: true, 
    slidesToShow: 5,
     slidesToScroll: 3,
     cssEase: 'linear',
     speed: 500,
     dots: false,
     centerMode: true,
     focusOnSelect: true,
     autoplay: true,
     autoplaySpeed: 2000,
     prevArrow:false,
     nextArrow:"<img class='slick-next' src='http://local.test/app/uploads/2019/05/arrow-point-to-right_copie.png'>"

     
   });
 });
  // Load Events
  $(document).ready(UTIL.loadEvents);
  $(function(){
    $('.single-item').slick({
      autoplay: true,
      infinite: true,
      speed: 500,
      fade: true,
      cssEase: 'linear',
      dots: true,
       prevArrow:false,
      nextArrow:false
    });
  });

  $('.menu-toggle').click( function() {
    $("#primary-menu").toggleClass("toggleClass");
  } );
  

})(jQuery); // Fully reference jQuery after this point.

